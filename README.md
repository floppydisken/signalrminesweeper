SignalRMinesweeper
==================
# About
A little game showcasing a simple use case for using SignalR. 
A game similar to minesweeper is contained in this repository.

## Game rules
Classic Minesweeper rules

# Status
Early stages, nothing fancy has been setup, just the bare minimums.

# TODO
- Checkout 


# Run backend 
Open Server/signalrminesweeper project in your favorite editor and run.

## Requirements
dotnet core 3.1


# Run frontend
This project uses nodejs 14. Open webclient/ and run `npm install` then `npm start`.
