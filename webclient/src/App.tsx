import React from "react";
import "./App.scss";
import Grid from "./components/Grid";
import CreateGameMenu from "./components/CreateGameMenu";
import Lobby from "./components/Lobby";
import MainMenu from "./components/MainMenu";
import MinesweeperApiProxy from "./MinesweeperApiProxy";
import { GameState } from "./util";
import Sandbox from "./sandbox/Sandbox";
import GameCanvas from "./components/GameCanvas";

export default class App extends React.Component<any, any> {
    api: MinesweeperApiProxy = MinesweeperApiProxy.instance;

    constructor(props: any) {
        super(props);

        this.state = {
            view: this.createMainMenu(),
        };
    }

    updateView(view: any) {
        this.setState((state: any) => {
            state.view = view;
            return { view: state.view };
        });
    }

    createMainMenu() {
        return (
            <MainMenu
                onCreateGame={this.createGame.bind(this)}
                onJoinGame={this.joinGame.bind(this)}
            />
        );
    }

    createGame() {
        this.setState({
            view: <CreateGameMenu onGameCreated={this.joinGame.bind(this)} />,
        });
    }

    joinGame(gameName: string) {
        this.api
            .joinGame(gameName)
            .then(async () => {
                this.setState({
                    view: (
                        <Lobby
                            gameName={gameName}
                            canStartGame={await this.api.amIGameOwner(gameName)}
                            onStartGame={this.startGame.bind(this)}
                            onGameStateChanged={async (
                                gameState: GameState
                            ) => {
                                console.log(gameState);
                                this.setState({
                                    view: await this.renderGrid(gameName),
                                });
                            }}
                        />
                    ),
                });
            })
            .catch((err: any) => {
                alert(`Failed with message: ${err}`);
            });
    }

    async renderGrid(gameName: string) {
        return (
            <GameCanvas gameName={gameName} size={await this.api.getGridSize(gameName)} />
            // <Grid
            //     gameName={gameName}
            //     // size={await this.api.getGridSize(gameName)}
            //     size={await this.api.getGridSize(gameName)}
            // />
        );
    }

    startGame(gameName: string) {
        this.api.startGame(gameName).then(async () => {
            this.setState({
                view: await this.renderGrid(gameName),
            });
        });
    }

    render() {
        return this.state.view;
        // return <Sandbox />
    }
}
