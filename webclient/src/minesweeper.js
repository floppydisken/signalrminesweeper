﻿"use strict";

const WIDTH = 600;
const HEIGHT = 600;

// SignalR config
//const connection = new signalR.HubConnectionBuilder().withUrl("index.html").build();
var connection = new signalR.HubConnectionBuilder()
    .configureLogging(signalR.LogLevel.Trace)
    .withUrl("/minesweeperhub")
    .build();

const gameName = document.getElementById("canvas").getAttribute("data-game-name") || "default";
let grid = null;
let boxSize = 20;

function make2DArray(size) {
    let outArray = new Array(size);

    for (let i = 0; i < outArray.length; ++i) {
        outArray[i] = new Array(size);
    }

    return outArray;
}

// Run on setup
function setup() {
    // Data setup

    // P5
    createCanvas(WIDTH, HEIGHT);
    frameRate(15);
    textAlign(CENTER, CENTER);
    noLoop(); // Don't draw until game has been joined

    // SIGNALR
    connection.start().then(() => {
        console.log("Connected to the hub");

        connection.on("SendRevealedCells", (cells) => {
            console.log("SendRevealedCells", cells);
            for (let i = 0; i < cells.length; ++i) {
                let cell = cells[i];
                grid[cell.x][cell.y] = cell;
            }
        });

        invokeJoinGame(gameName);
    });

}

function invokeJoinGame(gameName) {
    connection.invoke("JoinGame", gameName) // Join static game 0 atm
        .then(game => {
            grid = make2DArray(game.gridSize);
            boxSize = WIDTH / game.gridSize;
            loop(); // Start drawing
        })
        .catch(ex => console.log(ex));
}

// Tick every n
function draw() {
    background(240);
    drawGrid();
}

function drawGrid() {
    if (grid == null)
        return;

    for (let x = 0; x < grid.length; ++x) {
        for (let y = 0; y < grid[x].length; ++y) {
            let cell = grid[x][y];

            if (revealed(cell)) {
                fill(150);
            } else {
                fill(255);
            }

            rect(x * boxSize,
                y * boxSize,
                boxSize - 1,
                boxSize - 1);

            // TODO: A bit hackish
            if (revealed(cell)) {
                if (cell.isBomb) {
                    ellipseMode(CENTER);
                    let ellipseRadius = boxSize / 2;
                    let ellipseX = (x * boxSize) + (boxSize / 2);
                    let ellipseY = (y * boxSize) + (boxSize / 2);
                    ellipse(ellipseX, ellipseY, ellipseRadius);
                } else if (cell.nearbyBombs > 0) {
                    textAlign(CENTER, CENTER);
                    textSize(14);
                    textStyle(BOLD);
                    fill(255);
                    text(cell.nearbyBombs, x * boxSize + boxSize / 2, y * boxSize + boxSize / 2);
                }
                
            }
        }
    }
}

function revealed(cell) {
    return typeof(cell) !== "undefined";
}

function mousePressed() {
    if (grid == null)
        return;

    for (let x = 0; x < grid.length; ++x) {
        for (let y = 0; y < grid[x].length; ++y) {
            if (withinCell(x, y)) {
                connection.invoke("RevealCell", gameName, x, y)
                    .then((cells) => {
                        for (let i = 0; i < cells.length; ++i) {
                            let cell = cells[i];
                            grid[cell.x][cell.y] = cell;
                            console.log("Revealed cell");
                            console.log(cell);
                        }
                    })
                    .catch((err) => console.log(err.toString()));
            }
        }
    }
}

function withinCell(x, y) {
    return mouseX > x * boxSize &&
        mouseX < (x + 1) * boxSize &&
        mouseY > y * boxSize &&
        mouseY < (y + 1) * boxSize;
}
