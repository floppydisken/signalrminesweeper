import {
    LogLevel,
    HubConnectionBuilder,
    HubConnection,
    HubConnectionState,
} from "@microsoft/signalr";
import CellModel from "./models/CellModel";
import PlayerModel from "./models/PlayerModel";
import { Callback, TypedCallback, GameState } from "./util";
import LeaderboardModel, { LeaderboardEntry } from "./models/LeaderboardModel";

export default class MinesweeperApiProxy {
    private static _instance: MinesweeperApiProxy;
    private connection: HubConnection;

    private onMethodNames: {
        onPlayerJoined: string;
        onCellsRevealed: string;
        onGameStateChanged: string;
        onLeaderboardUpdate: string;
    };

    constructor() {
        this.connection = new HubConnectionBuilder()
            .configureLogging(LogLevel.Trace)
            // TODO: Consider online version also
            .withUrl("http://localhost:5000/minesweeperhub")
            // .withUrl("https://minesweeperoyale.azurewebsites.net/minesweeperhub")
            .withAutomaticReconnect()
            .build();

        this.onMethodNames = {
            onPlayerJoined: "SendPlayerJoined",
            onCellsRevealed: "SendRevealedCells",
            onGameStateChanged: "SendGameStateChange",
            onLeaderboardUpdate: "SendLeaderboardUpdate",
        };
    }

    public static get instance() {
        if (!MinesweeperApiProxy._instance) {
            MinesweeperApiProxy._instance = new MinesweeperApiProxy();
        }
        return MinesweeperApiProxy._instance;
    }

    private on(methodName: string, callback: Callback) {
        this.connection.on(methodName, callback);
    }

    onPlayerJoined(callback: TypedCallback<PlayerModel>) {
        this.on(this.onMethodNames.onPlayerJoined, callback);
    }

    onCellsRevealed(callback: TypedCallback<CellModel[]>) {
        this.on(this.onMethodNames.onCellsRevealed, callback);
    }

    onGameStateChanged(callback: TypedCallback<GameState>) {
        this.on(this.onMethodNames.onGameStateChanged, callback);
    }

    onLeaderboardUpdate(callback: Callback) {
        this.on(this.onMethodNames.onLeaderboardUpdate, callback);
    }

    private off(methodName: string) {
        this.connection.off(methodName);
    }

    offPlayerJoined() {
        this.off(this.onMethodNames.onPlayerJoined);
    }

    offCellsRevealed() {
        this.off(this.onMethodNames.onCellsRevealed);
    }

    async makeSureIsConnected() {
        if (this.connection.state !== HubConnectionState.Connected)
            await this.connection.start();
    }

    async joinGame(gameName: string) {
        await this.makeSureIsConnected();
        return await this.connection.invoke("JoinGame", gameName);
    }

    async createGame(gameName: string, entropy: number, gridSize: number) {
        await this.makeSureIsConnected();
        return await this.connection.invoke(
            "CreateGame",
            gameName,
            entropy,
            gridSize
        );
    }

    async startGame(gameName: string) {
        await this.makeSureIsConnected();
        return await this.connection.invoke("StartGame", gameName);
    }

    async setName(name: string) {
        await this.makeSureIsConnected();
        return await this.connection.invoke("SetPlayerName", name);
    }

    async amIGameOwner(gameName: string) {
        return await this.connection.invoke("AmIGameOwner", gameName);
    }

    async getGridSize(gameName: string) {
        return await this.connection.invoke("GetGridSize", gameName);
    }

    async revealCell(gameName: string, x: number, y: number) {
        await this.connection
            .invoke("RevealCell", gameName, x, y)
            .catch((err) => {
                console.log(err);
            });
    }

    async getPlayerListOfGame(gameName: string): Promise<PlayerModel[]> {
        const playersAsJson = await this.connection
            .invoke("GetPlayerList", gameName)
            .catch((err) => {
                console.log(err);
            });

        const deserializedPlayers = playersAsJson.map((paj: any) => {
            return new PlayerModel(
                paj.id,
                paj.alias,
                paj.isDead
            )
        });

        return deserializedPlayers
    }

    async getLeaderboard(gameName: string) {
        const leaderboardAsJson = await this.connection
            .invoke("GetLeaderboard", gameName)
            .catch(console.log);
        const playerList = await this.getPlayerListOfGame(gameName);
        const deserializedLeaderboard: LeaderboardEntry[] = Object.entries<number>(leaderboardAsJson)
            .map(([key, val]) => {
                const player = playerList.find((p) => p.id === key);
                return {player: new PlayerModel(key, player?.alias ?? "Anon", !!player?.isDead), score: val}
            });
        return new LeaderboardModel(deserializedLeaderboard);
    }
}
