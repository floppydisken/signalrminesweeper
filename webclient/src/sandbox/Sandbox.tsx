import React from "react";
import Grid from "../components/Grid";
import GameCanvas from "../components/GameCanvas";

const Sandbox: React.FC = () => {
    return (
        <div>
            <GameCanvas gameName="Lol" size={10} />
            {/* <Grid gameName="Lol" size={10} /> */}
        </div>
    );
};

export default Sandbox;
