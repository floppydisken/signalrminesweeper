export default class CellModel {
    constructor(
        public x: number,
        public y: number,
        public nearbyBombs: number,
        public revealed: boolean = false,
        public isBomb: boolean = false,
    ) { }
}