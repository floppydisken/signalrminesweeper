import PlayerModel from "./PlayerModel"
import { Dictionary } from "../util";

export type LeaderboardEntry = {
    player: PlayerModel,
    score: number
}

export default class LeaderboardModel {
    entries: Dictionary<LeaderboardEntry>;

    constructor(leaderboardEntries: LeaderboardEntry[] = []) {
        this.entries = {};
        leaderboardEntries.forEach((entry) => {
            this.entries[entry.player.id] = entry
        })
    }

    updateScore(playerId: string, score: number) {
        this.entries[playerId].score += score;
    }

    getEntries() {
        return Object.values(this.entries);
    }
}