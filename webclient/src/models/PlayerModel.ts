
export default class PlayerModel {
    constructor(
        public id: string,
        public alias: string,
        public isDead: boolean
    ) {}
}