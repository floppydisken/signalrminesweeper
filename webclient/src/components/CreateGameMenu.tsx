import "./Menu.scss"
import * as React from "react";
import MinesweeperApiProxy from "../MinesweeperApiProxy";

interface Props {
    onGameCreated: (gameName: string) => void;
}

interface State {
    gameName: string;
    entropy: number;
    gridSize: number;
}

export default class CreateGameMenu extends React.Component<Props, State> {
    api: MinesweeperApiProxy = MinesweeperApiProxy.instance;

    constructor(props: Props) {
        super(props);

        this.state = {
            gameName: "",
            entropy: 10,
            gridSize: 20,
        }
    }

    createGame() {
        const { gameName, entropy, gridSize } = this.state;
        const { onGameCreated } = this.props;

        this.api.createGame(
            gameName,
            entropy,
            gridSize
        ).then(() => {
            onGameCreated(gameName);
        }).catch(err => {
            alert(`Failed with error message: ${err}`)
        });
    }

    render() {
        return (
            <div className={"container pure-form"}>
                <div className={"pure-g"}>
                    <div className={"spaced pure-u-1"}>
                        <label className={"pure-u-2-5"}>Game name: </label>
                        <input placeholder={"Game name"}
                               className={"pure-u-3-5 pure-md-1-3"}
                               value={this.state.gameName}
                               onChange={(event) => this.setState({gameName: event.target.value})}
                               type={"text"}
                        />
                    </div>

                    <div className={"spaced pure-u-1"}>
                        <label className={"pure-u-2-5"}>Entropy: </label>
                        <input placeholder={"Bomb spawn chance"}
                               className={"pure-u-3-5"}
                               type={"range"}
                               value={this.state.entropy}
                               // onChange={(event) => this.setState({entropy: event.target.value})}
                               onChange={(event) => this.setState({entropy: parseInt(event.target.value)})}
                        />
                    </div>

                    <div className={"spaced pure-u-1"}>
                        <label className={"pure-u-2-5"}>Grid size: </label>
                        <input placeholder={"Grid size"}
                               className={"pure-u-3-5"}
                               type={"number"}
                               value={this.state.gridSize}
                               onChange={(event) => this.setState({gridSize: parseInt(event.target.value)})}
                        />
                    </div>

                    <div className={"spaced pure-u-1"}>
                        <button className={"pure-u-1-1 pure-button"} onClick={this.createGame.bind(this)}>Create Game</button>
                    </div>
                </div>
            </div>
        )
    }
}