import "./Cell.scss";
import * as React from "react";
import "./assets/bomb.svg";
import MinesweeperApiProxy from "../MinesweeperApiProxy";
import CellModel from "../models/CellModel";

interface State {
    revealed: boolean;
    isBomb: boolean;
    nearbyBombs: number;
}

interface Props {
    gameName: string;
    x: number;
    y: number;
    // Cell size
    size: number | string;
}

export default class Cell extends React.Component<Props, State> {
    api: MinesweeperApiProxy = MinesweeperApiProxy.instance;

    constructor(props: Props) {
        super(props);

        this.state = {
            revealed: false,
            isBomb: false,
            nearbyBombs: 0,
        };

        // TODO: This is disgusting fix in the future. It scales horribly.
        this.api.onCellsRevealed(this.onReveal.bind(this));
    }

    componentWillUnmount() {
        this.api.offCellsRevealed();
    }

    getClassNames() {
        const { nearbyBombs, revealed, isBomb } = this.state;
        let classNames = [];

        classNames.push("cell");

        if (revealed) {
            classNames.push("revealed");

            switch (nearbyBombs) {
                case 1:
                    classNames.push("blue");
                    break;
                case 2:
                    classNames.push("green");
                    break;
                default:
                    classNames.push("red");
                    break;
            }
        }
        if (isBomb) {
            classNames.push("bomb");
        }

        return classNames.join(" ");
    }

    async onReveal(cells: CellModel[]) {
        // TODO: Sloppy solution. Revamp in the future. At the moment this is O(N*N)
        for (let i = 0; i < cells.length; ++i) {
            let cell = cells[i];
            if (cell.x === this.props.x && cell.y === this.props.y) {
                this.update(cell);
            }
        }
    }

    async update(cell: CellModel) {
        this.setState({
            revealed: cell.revealed,
            isBomb: cell.isBomb,
            nearbyBombs: cell.nearbyBombs,
        });
    }

    async onClick(event: React.MouseEvent) {
        await this.api.revealCell(
            this.props.gameName,
            this.props.x,
            this.props.y
        );
        // this.setState({ revealed: true, isBomb: Math.random() > 0.5 })
    }

    renderButton() {
        const style: React.CSSProperties = {
            width: `${this.props.size}`,
            height: `${this.props.size}`,
        };

        const ifBombDontDisplayNearbyBombs = () => {
            if (this.state.isBomb) return "";
            if (this.state.nearbyBombs > 0) return this.state.nearbyBombs;
        };

        if (this.state.revealed) {
            return (
                <button
                    style={style}
                    onDrag={(e) => e.preventDefault()}
                    className={this.getClassNames()}
                    onClick={this.onClick.bind(this)}
                >
                    {ifBombDontDisplayNearbyBombs()}
                </button>
            );
        }

        return (
            <button
                style={style}
                onDrag={(e) => e.preventDefault()}
                className={this.getClassNames()}
                onClick={this.onClick.bind(this)}
            />
        );
    }

    render() {
        return this.renderButton();
    }
}
