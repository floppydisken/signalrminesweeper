import "./Base.scss";
import "purecss/build/pure.css";
import React, {useState} from "react";
import MinesweeperApiProxy from "../MinesweeperApiProxy";

interface Props {
  onJoinGame: (name: string) => void;
  onCreateGame: () => void;
}

const MainMenu: React.FC<Props> = ({onJoinGame, onCreateGame}) => {
  const api = MinesweeperApiProxy.instance;
  const [gameToJoin, setGameToJoin] = useState("");
  const [name, setName] = useState("");
  return (
    <div className={"container"}>
      <div className={"pure-form pure-form-aligned"}>
        <div className={"pure-g"}>
          <button
            className={"pure-u-1 pure-button spaced"}
            onClick={onCreateGame}
          >
            Create Game
          </button>
          <label className={"pure-u-1-5"}>Game to join</label>
          <input
            className={"pure-u-3-5"}
            placeholder={"Name of the game"}
            type={"text"}
            onChange={(event) => setGameToJoin(event.target.value)}
          />
          <button
            className={"pure-u-1-5 pure-button"}
            onClick={() => onJoinGame(gameToJoin)}
          >
            Join Game
          </button>
          <input
            placeholder="Insert name"
            className="pure-u-4-5" 
            onChange={(event) => setName(event.target.value)}
            />
          <button 
            className="pure-u-1-5 pure-button"
            onClick={() => { api.setName(name); }}
            >
            Set Name
          </button>
        </div>
      </div>
    </div>
  );
};

export default MainMenu;
