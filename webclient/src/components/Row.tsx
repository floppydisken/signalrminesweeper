import "./Row.scss";
import * as React from "react";
import Cell from "./Cell";
import { useState } from "react";

interface Props {
    gameName: string;
    size: number;
    y: number;
}

const Row: React.FC<Props> = ({ gameName, size, y }) => {
    const createCells = () => {
        let cells = [];
        for (let x = 0; x < size; ++x) {
            cells.push(
                <Cell size={`2rem`} gameName={gameName} x={x} y={y} key={`${x},${y}`} />
            );
        }
        return cells;
    };

    const [cells] = useState(createCells());

    return <div className={"row"}>{cells}</div>;
};

export default Row;
