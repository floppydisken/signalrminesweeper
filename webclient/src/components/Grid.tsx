import "./Grid.scss";
import * as React from "react";
import Row from "./Row";
import MinesweeperApiProxy from "../MinesweeperApiProxy";
import LeaderboardModel from "../models/LeaderboardModel";
import Cell from "./Cell";

interface Props {
    gameName: string;
    size: number;
}

interface State {
    leaderboard: LeaderboardModel;
    rows: any[];
    cells: any[];
}

class Grid extends React.Component<Props, State> {
    api = MinesweeperApiProxy.instance;

    constructor(props: Props) {
        super(props);
        this.state = {
            leaderboard: new LeaderboardModel(),
            rows: [],
            cells: [],
        }
    }

    updateLeaderboard(player: any, score: any) {
        this.setState(state => {
            state.leaderboard.updateScore(player.id, score);
            return {leaderboard: new LeaderboardModel(state.leaderboard.getEntries())}
        });
    }

    createRows(amount: number) {
        // Acts as a count for both x and y axis
        // Results in a quadratic grid
        // TODO: Make this one dimensional and utilize css grids.
        const { gameName } = this.props;
        let rows = [];
        for (let y = 0; y < amount; ++y) {
            rows.push(<Row key={y} gameName={gameName} y={y} size={amount} />);
        }
        return rows;
    }

    createCells(width: number, height: number = width) {
        let cells = [];

        for (let i = 0; i < width * height; ++i) {
            const x = i % width;
            const y = i / width;
            cells.push({x: x, y: y, });
        }

        return cells;
    }

    async componentDidMount() {
        const { gameName, size } = this.props;
        this.api.onLeaderboardUpdate(this.updateLeaderboard.bind(this));
        this.setState({
            cells: this.createCells(size),
            // leaderboard: await this.api.getLeaderboard(gameName)
        });
    }

    // render() {
    //     const { leaderboard, rows } = this.state;
    //     const { gameName } = this.props;

    //     return (
    //         <div>
    //             <h1 className={"title"}>{gameName}</h1>
    //             <div className={"grid"} >
    //                 {rows}
    //             </div>
    //             <div className={"leaderboard"}>
    //                 <ul>
    //                     {leaderboard.getEntries().map((entry, index) => (
    //                         // TODO: Doesnt work. Supposedly never notified that we're dead.
    //                         entry.player.isDead 
    //                         ? <li key={index}><span style={{color: "red"}}>{`${entry.player.alias}: ${entry.score}`}</span></li> 
    //                         : <li key={index}>{`${entry.player.alias}: ${entry.score}`}</li>
    //                     ))}
    //                 </ul>
    //             </div>
    //         </div>
    //     );
    // }

    render() {
        const { leaderboard, cells } = this.state;
        const { gameName, size } = this.props;

        const cellSize = 2;
        const style: React.CSSProperties = {
            display: "grid",
            gridTemplateColumns: `repeat(${size}, 1fr)`,
            columnGap: 0,
            width: `${cellSize * size}rem`,
            height: `${cellSize * size}rem`,
            position: "absolute",
        }
        const wrappedCells = cells.map((c, i) => <Cell size={`${cellSize}rem`} key={i} gameName={gameName} x={c.x} y={c.y} />)

        return (
            <div style={style}>
                {wrappedCells}
            </div>
        );
    }
}

export default Grid;
