import styles from "./GameCanvas.module.scss";
import React, { useEffect, useState } from "react";
import * as PIXI from "pixi.js";

import cellImage from "./assets/cell.png";
import cellInvertedImage from "./assets/cell-inverted.png";
import cellInvertedRedImage from "./assets/cell-inverted-red.png";
import bombImage from "./assets/bomb.png";
import MinesweeperApiProxy from "../MinesweeperApiProxy";
import LeaderboardModel from "../models/LeaderboardModel";

const TEXTURES = {
    cell: PIXI.Texture.from(cellImage),
    cellInverted: PIXI.Texture.from(cellInvertedImage),
    cellInvertedRed: PIXI.Texture.from(cellInvertedRedImage),
    bomb: PIXI.Texture.from(bombImage),
};

enum ButtonState {
    Dragging,
    NotDragging,
}

enum CellState {
    Hidden,
    Revealed,
    Bomb,
}

class Cell extends PIXI.Sprite {
    _onClick: (cell: Cell) => void;
    _state: CellState = CellState.Hidden;
    nearbyBombs: number = 0;

    constructor(
        app: PIXI.Application,
        public sprites: {
            hidden: PIXI.Texture;
            revealed: PIXI.Texture;
            bomb: PIXI.Texture;
        },
        public indexX: number,
        public indexY: number,
        onClick: (cell: Cell) => void
    ) {
        super(sprites.hidden);
        this.sprites = sprites;
        this._onClick = onClick;
        const size = 32;
        this.width = size;
        this.height = size;
        this.position.x = this.width * indexX;
        this.position.y = this.width * indexY;

        this.interactive = true;
        this.on("click", this.onClick);
    }

    set state(state: CellState) {
        this._state = state;
        this.changeState(this._state);
    }

    onClick(e: PIXI.InteractionData) {
        if (currentButtonState !== ButtonState.Dragging) {
            this._onClick(this);
        }
    }

    reveal(isBomb: boolean, nearbyBombs: number) {
        if (isBomb) this.state = CellState.Bomb;
        else this.state = CellState.Revealed;

        this.nearbyBombs = nearbyBombs;

        if (this.nearbyBombs > 0) {
            let color = 0x000000;

            if (this.nearbyBombs === 1) {
                color = 0x0000FF;
            } else if (this.nearbyBombs === 2) {
                color = 0x00FF00;
            } else if (this.nearbyBombs === 3) {
                color = 0xFF0000;
            }

            const textStyle: PIXI.TextStyle = new PIXI.TextStyle({
                fontSize: 50,
                fontWeight: "bold",
                fill: color,
                align: "center",
            });
            
            const text = new PIXI.Text(`${this.nearbyBombs}`, textStyle);
            text.anchor.set(0.5);
            text.position.x = this.texture.width / 2;
            text.position.y = this.texture.height / 2;
            this.addChild(text);
        }
    }

    changeState(state: CellState) {
        switch (state) {
            case CellState.Revealed:
                this.texture = this.sprites.revealed;
                break;
            case CellState.Hidden:
                this.texture = this.sprites.hidden;
                break;
            case CellState.Bomb:
                this.texture = this.sprites.bomb;
                break;
            default:
                break;
        }
    }
}

const renderGrid = (app: PIXI.Application, size: number, onCellClicked: (cell: Cell) => void) => {
    const cells = [];
    const totalCells = size * size; // Essentially "width * height = v"
    for (let i = 0; i < totalCells; ++i) {
        const width = size;
        const x = i % width;
        const y = Math.floor(i / width);
        cells.push(
            new Cell(
                app,
                {
                    hidden: TEXTURES.cell,
                    revealed: TEXTURES.cellInverted,
                    bomb: TEXTURES.cellInvertedRed,
                },
                x,
                y,
                onCellClicked
            )
        );
    }
    const gridContainer = new DraggableContainer();
    cells.forEach((c) => gridContainer.addChild(c));
    return gridContainer;
};

class DraggableContainer extends PIXI.Container {}

class WorldPlane extends PIXI.Graphics {
    dragging: boolean = false;
    mouseDown: boolean = false;
    previousX: number | undefined;
    previousY: number | undefined;

    draggingDistance: number = 0;
    draggingThreshold: number = 10;

    constructor(width: number, height: number = width) {
        super();

        const drawX = -width;
        const drawY = -height;
        const drawWidth = width * 3;
        const drawHeight = height * 3;

        this.beginFill(0x000000, 0.5);
        this.drawRect(drawX, drawY, drawWidth, drawHeight);
        this.endFill();

        this.interactive = true;

        this.on("mousedown", this.onDragStart);
        this.on("mousedownoutside", this.onDragStart);
        this.on("mousemove", this.onDrag);
        this.on("mouseup", this.onDragEnd);
        this.on("mouseupoutside", this.onDragEnd);

        this.on("touchstart", this.onDragStart);
        this.on("touchmove", this.onDrag);
        this.on("touchend", this.onDragEnd);
        this.on("touchendoutside", this.onDragEnd);

        this.on("pointermove", this.onDrag);
        this.on("pointerdown", this.onDragStart);
        this.on("pointerup", this.onDragEnd);
        this.on("pointerupoutside", this.onDragEnd);
    }

    onDragStart(e: PIXI.InteractionEvent) {
        this.mouseDown = true;
        this.draggingDistance = 0;
    }

    onDragEnd() {
        this.mouseDown = false;
        currentButtonState = ButtonState.NotDragging;

        // We need to reset these, so we dont get a jumpy plane.
        this.previousY = undefined;
        this.previousX = undefined;
    }

    onDrag(e: PIXI.InteractionEvent) {
        if (this.mouseDown) {
            const newPosition = e.data.getLocalPosition(this.parent);

            if (this.draggingDistance > this.draggingThreshold) {
                currentButtonState = ButtonState.Dragging;
            }

            this.dragging = true;

            if (this.dragging && this.previousX && this.previousY) {
                const differenceX = this.previousX - newPosition.x;
                const differenceY = this.previousY - newPosition.y;

                this.position.x -= differenceX;
                this.position.y -= differenceY;

                this.draggingDistance +=
                    Math.abs(differenceX) + Math.abs(differenceY);
            }
            this.previousX = newPosition.x;
            this.previousY = newPosition.y;
        }
    }
}


let currentButtonState: ButtonState = ButtonState.NotDragging;

interface Props {
    gameName: string;
    size: number;
}

interface State {
    leaderboard: LeaderboardModel;
    gridContainer?: PIXI.Container;
}

class GameCanvas extends React.Component<Props, State> {
    api: MinesweeperApiProxy;

    constructor(props: Props) {
        super(props);
        const { size } = props;

        this.state = {
            leaderboard: new LeaderboardModel(),
            gridContainer: undefined,
        }

        this.api = MinesweeperApiProxy.instance;

        this.api.onCellsRevealed((revealedCells) => {
            const { gridContainer } = this.state;
            console.log(revealedCells);

            if (gridContainer) {
                revealedCells.forEach(rc => {
                    const children = gridContainer.children as Cell[];
                    const width = children.length / size;
                    const height = width;
                    const foundCell = children[(rc.y * height) + rc.x];

                    foundCell.reveal(rc.isBomb, rc.nearbyBombs);
                });
            }
        });
    }

    async componentDidMount() {
        const { gameName, size } = this.props;

        this.setState({leaderboard: await this.api.getLeaderboard(gameName)});

        this.api.onLeaderboardUpdate((player: any, score: any) => {
            this.setState(state => {
                state.leaderboard?.updateScore(player.id, score);
                return { leaderboard: new LeaderboardModel(state.leaderboard.getEntries()) }
            })
            
        });

        const canv = document.getElementById("canvas");
        const app = new PIXI.Application({
            backgroundColor: 0xffff,
        });
        app.resizeTo = canv ?? window;

        canv?.appendChild(app.view);
        window.onresize = () => {
            app.resize();
        };

        const width = canv?.offsetWidth ?? window.innerWidth;
        const height = canv?.offsetHeight ?? window.innerHeight;
        const grid = renderGrid(app, size, (cell) => {
            this.api.revealCell(gameName, cell.indexX, cell.indexY)
        });
        grid.position.set(
            width / 2 - grid.width / 2,
            height / 2 - grid.height / 2
        );
        this.setState({gridContainer: grid});
        // TODO: If the grid is too large, the WorldPlane doesnt compensate. This needs to be fixed.
        const plane = new WorldPlane(
            width > grid.width ? width : grid.width,
            height > grid.height ? height : grid.height
        );

        plane.addChild(grid);
        app.stage.addChild(plane);
        app.render();

    }

    render() {
        const { leaderboard } = this.state;

        return (
            <div className={styles.GameCanvas}>
                <div className={styles.Leaderboard}>
                    <ul>
                        {leaderboard?.getEntries().map((entry, index) => (
                            // TODO: Doesnt work. Supposedly never notified that we're dead.
                            entry.player.isDead 
                            ? <li key={index}><span style={{color: "red"}}>{`${entry.player.alias}: ${entry.score}`}</span></li> 
                            : <li key={index}>{`${entry.player.alias}: ${entry.score}`}</li>
                        ))}
                    </ul>
                </div>
                <div id="canvas" style={{ width: "100vw", height: "100vh" }} />
            </div>
        );
    }
}

// const GameCanvas: React.FC<Props> = ({ children, gameName, size }) => {
//     const [leaderboard, setLeaderboard] = useState<LeaderboardModel | undefined>();

//     const api = MinesweeperApiProxy.instance;

//     const tick = (...params: any[]) => {};
//     const setup = (app: PIXI.Application) => {
//         app.loader.onError.add(console.log);
//         app.ticker.add(tick);
//     };

//     useEffect(() => {
//         async function setupLeaderboard() {
//             setLeaderboard(await api.getLeaderboard(gameName));
//         }
//         setupLeaderboard();

//         api.onLeaderboardUpdate((player: any, score: any) => {
//             leaderboard?.updateScore(player.id, score);
//         });

//         const canv = document.getElementById("canvas");
//         const app = new PIXI.Application({
//             backgroundColor: 0xffff,
//         });
//         app.resizeTo = canv ?? window;

//         canv?.appendChild(app.view);
//         window.onresize = () => {
//             app.resize();
//         };

//         setup(app);
//         const width = canv?.offsetWidth ?? window.innerWidth;
//         const height = canv?.offsetHeight ?? window.innerHeight;
//         const grid = renderGrid(app, size, (cell) => {
//             api.revealCell(gameName, cell.indexX, cell.indexY)
//         });
//         grid.position.set(
//             width / 2 - grid.width / 2,
//             height / 2 - grid.height / 2
//         );
//         // TODO: If the grid is too large, the WorldPlane doesnt compensate. This needs to be fixed.
//         const plane = new WorldPlane(
//             width > grid.width ? width : grid.width,
//             height > grid.height ? height : grid.height
//         );

//         plane.addChild(grid);
//         app.stage.addChild(plane);
//         app.render();

//         api.onCellsRevealed((revealedCells) => {
//             console.log(revealedCells);

//             revealedCells.forEach(rc => {
//                 const children = grid.children as Cell[];
//                 const width = children.length / size;
//                 const height = width;
//                 const foundCell = children[(rc.y * height) + rc.x];

//                 foundCell.reveal(rc.isBomb, rc.nearbyBombs);
//             });
//         });

//         return () => {
//             api.offCellsRevealed();
//         }
//     }, []);

//     return (
//         <div className={styles.GameCanvas}>
//             <div className={styles.Leaderboard}>
//                 <ul>
//                     {leaderboard?.getEntries().map((entry, index) => (
//                         // TODO: Doesnt work. Supposedly never notified that we're dead.
//                         entry.player.isDead 
//                         ? <li key={index}><span style={{color: "red"}}>{`${entry.player.alias}: ${entry.score}`}</span></li> 
//                         : <li key={index}>{`${entry.player.alias}: ${entry.score}`}</li>
//                     ))}
//                 </ul>
//             </div>
//             <div id="canvas" style={{ width: "100vw", height: "100vh" }} />
//         </div>
//     );
// };

export default GameCanvas;
