import "./Base.scss";
import "purecss/build/pure.css";
import * as React from "react";
import MinesweeperApiProxy from "../MinesweeperApiProxy";
import PlayerModel from "../models/PlayerModel";
import { Callback, TypedCallback, GameState } from "../util";

interface State {
    players: PlayerModel[];
}

interface Props {
    gameName: string;
    canStartGame: boolean;
    onStartGame: Callback;
    onGameStateChanged: TypedCallback<GameState>;
}

export default class Lobby extends React.Component<Props, State> {
    api: MinesweeperApiProxy = MinesweeperApiProxy.instance;

    constructor(props: any) {
        super(props);

        this.state = {
            players: [],
        };

        this.api.onPlayerJoined(this.onPlayerJoined.bind(this));
        this.api.onGameStateChanged(this.props.onGameStateChanged);
    }

    onPlayerJoined(player: PlayerModel) {
        this.setState((state) => {
            const exists = !!state.players.find((p) => p.id === player.id);
            if (!exists) return { players: [...state.players, player] };
        });
    }

    componentWillUnmount() {
        this.api.offPlayerJoined();
    }

    async componentDidMount() {
        const { gameName } = this.props;
        this.setState({players: await this.api.getPlayerListOfGame(gameName)});
    }

    render() {
        const { gameName, canStartGame } = this.props;
        const { players } = this.state;
        const wrappedPlayers = players.map((player, index) => (
            <li key={index}>{player.alias || player.id}</li>
        ));

        return (
            <div className={"container"}>
                <h1>Tell your friends to join game with name <span style={{color: "lightgreen"}}>{gameName}</span></h1>
                <h2 className={"pure-u-1"}>Players:</h2>
                <ul>{wrappedPlayers}</ul>
                {canStartGame ? (
                    <button onClick={() => this.props.onStartGame(gameName)}>
                        Start game
                    </button>
                ) : (
                   <p>Waiting for owner to start the game...</p> 
                )}

            </div>
        );
    }
}
