export type Callback = (...args: any[]) => void;
export type TypedCallback<T> = (args: T) => void;

export type GameState = "NotStarted"
    | "Running"
    | "GameOver"


export interface Dictionary<T> {
    [key: string]: T
}