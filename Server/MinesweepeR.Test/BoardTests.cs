using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace MinesweepeR.Test
{
    public class BoardTests
    {
        private Board board;
        private int size;
        
        [SetUp]
        public void Setup()
        {
            size = 40;
            board = new Board(size);
        }

        [Test]
        public void GivenCellIsWithinBounds_WhenRevealingTheCell_ThenReturnAtleastThatCell()
        {
            IEnumerable<Cell> cells = board.RevealCell(1, 1);
            
            Assert.True(cells.Any());
        }

        [Test]
        public void GivenCellIsOutOfBounds_WhenRevealingTheCell_ThenThrowAnException()
        {
            Assert.Throws<IndexOutOfRangeException>(() => board.RevealCell(-1, -1));
        }
    }
}