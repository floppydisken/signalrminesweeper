using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Converters;

namespace MinesweepeR
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSignalR(config =>
            {
                config.ClientTimeoutInterval = TimeSpan.FromMinutes(1);
                config.KeepAliveInterval = TimeSpan.FromSeconds(30);
            }).AddJsonProtocol(config =>
            {
                config.PayloadSerializerOptions.Converters.Add(new JsonStringEnumConverter());
            });
            
            services.AddCors();
            services.AddSingleton<IRegister<string, Player>, Register<Player>>();
            services.AddSingleton<IRegister<string, Game>, Register<Game>>();
            services.AddSingleton(provider => new MinesweeperHub(
                provider.GetService<IRegister<string, Player>>(),
                provider.GetService<IRegister<string, Game>>()
            ));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app)
        {
            app.UseRouting();
            app.UseCors(builder =>
            {
                builder.AllowCredentials()
                    .AllowAnyHeader()
                    .AllowAnyMethod()
                    .WithOrigins("http://localhost:3000");
            });
                
            app.UseEndpoints(routes =>
            {
                routes.MapHub<MinesweeperHub>("/minesweeperhub");
            });
        }
    }
}
