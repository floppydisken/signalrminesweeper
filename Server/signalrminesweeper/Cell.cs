﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MinesweepeR
{
    [JsonObject(MemberSerialization.OptOut)]
    public class Cell
    {
        public int X { get; private set; }
        public int Y { get; private set; }
        public bool IsBomb { get; set; }
        public bool Revealed { get; set; }
        public int NearbyBombs { get; set; }

        public Cell(int x, int y, double chanceOfBeingBomb = 0.1 /* between 0.0 to 1.0 */)
        {
            X = x;
            Y = y;

            if (new Random().NextDouble() < chanceOfBeingBomb)
                IsBomb = true;
        }

        public void Reveal(int nearbyBombs)
        {
            NearbyBombs = nearbyBombs;
            Revealed = true;
        }
    }
}
