﻿using System;

namespace MinesweepeR
{
    public class GameException : Exception
    {
        public Game.State GameState { get; }

        public GameException(Game.State gameState, string message) : base(message)
        {
            GameState = gameState;
        }
        
        public GameException(Game.State gameState) : this(gameState, $"Failed because of wrong game state: {gameState}") { }
    }
}