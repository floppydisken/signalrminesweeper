﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MinesweepeR
{
    public class Register<T> : IRegister<string, T>
    {
        private IDictionary<string, T> register = new ConcurrentDictionary<string, T>();

        public T this[string uid]
        {
            get => register[uid];
            set => register[uid] = value;
        }

        public bool Contains(string uid)
        {
            return register.ContainsKey(uid);
        }

        public void Remove(string uid)
        {
            register.Remove(uid);
        }
    }
}
