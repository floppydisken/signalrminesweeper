﻿using System.Collections;
using System.Collections.Generic;

namespace MinesweepeR
{
    public interface IGameObserver
    {
        void OnGameStateChange(string gameName, Game.State gameState);
        void OnCellsRevealed(string gameName, IEnumerable<Cell> cells);
        void OnPlayerJoined(string gameName, Player player);
        void OnLeaderboardUpdate(string gameName, Player player, int score);
    }
}