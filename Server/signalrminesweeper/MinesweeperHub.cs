﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace MinesweepeR
{
    public class MinesweeperHub : Hub<IPlayerClient>, IGameObserver
    {
#region HubImplementation
        private readonly IRegister<string, Player> players;
        private readonly IRegister<string, Game> games;

        public MinesweeperHub(IRegister<string, Player> players, IRegister<string, Game> games)
        {
            this.players = players;
            this.games = games;
        }

        public override Task OnConnectedAsync()
        {
            var player = new Player { Id = Context.ConnectionId };
            players[Context.ConnectionId] = player;
            return base.OnConnectedAsync();
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            players.Remove(Context.ConnectionId);
            return base.OnDisconnectedAsync(exception);
        }

        public async Task<Game> CreateGame(string gameName, int entropy, int gridSize)
        {
            return await Task.Run(() =>
            {
                try
                {
                    if (string.IsNullOrWhiteSpace(gameName)) 
                        throw new HubException("Game name cannot be empty.");

                    if (!players.Contains(Context.ConnectionId))
                        players[Context.ConnectionId] = new Player { Id = Context.ConnectionId };
                    
                    var game = new Game(gameName, entropy, gridSize, GetPlayer(), this);
                    game.JoinGame(GetPlayer());

                    if (!games.Contains(gameName) || games[gameName].IsDone())
                    {
                        games[gameName] = game;
                    }
                    else
                    {
                        throw new HubException("Game with that name already exists");
                    }
                    return game;
                }
                catch (Exception e)
                {
                    throw new HubException($"Failed to create game {e.Message}");
                }
            });
        }

        public Task SetPlayerName(string name)
            => Task.Run(() =>
            {
                if (players[Context.ConnectionId] != null)
                {
                    players[Context.ConnectionId].Alias = name;
                }
            });

        public Task LeaveGame(string gameName) 
            => Task.Run(() => RunActionOnGame(gameName, (game) =>
            {
                Groups.RemoveFromGroupAsync(GetPlayer().Id, gameName);
                game.LeaveGame(GetPlayer());
            }));

        public Task JoinGame(string gameName) 
            => Task.Run(() => RunActionOnGame(gameName, (game) =>
            {
                Groups.AddToGroupAsync(GetPlayer().Id, gameName);
                game.JoinGame(GetPlayer());
            }));
        
        public Task StartGame(string gameName) 
            => Task.Run(() => RunActionOnGame(gameName, (game) => game.StartGame(GetPlayer())));

        public Task RevealCell(string gameName, int x, int y) 
            => Task.Run(() => RunActionOnGame(gameName, (game) => game.RevealCell(GetPlayer(), x, y)));

        public Task<bool> AmIGameOwner(string gameName) 
            => Task.Run(() => RunFuncOnGame(gameName, (game) => game.IsOwner(GetPlayer())));

        public Task<int> GetGridSize(string gameName) 
            => Task.Run(() => RunFuncOnGame(gameName, (game) => games[gameName].BoardSize));

        public Task<IEnumerable<Player>> GetPlayerList(string gameName)
            => Task.Run(() => RunFuncOnGame(gameName, (game) => games[gameName].Players));

        public Task<IDictionary<string, int>> GetLeaderboard(string gameName)
            => Task.Run(() => RunFuncOnGame(gameName, (game) => game.Leaderboard.Entries));

        private Task RunActionOnGame(string gameName, Action<Game> action)
        {
            return Task.Run(() =>
            {
                if (games.Contains(gameName))
                {
                    try
                    {
                        action(games[gameName]);
                    }
                    catch (GameException e)
                    {
                        throw new HubException($"Action on game with name {gameName} failed with message {e.Message}");
                    }
                }
                else
                {
                    throw new HubException("Game with that name doesnt exist.");
                }
            });
        }
        
        private Task<T> RunFuncOnGame<T>(string gameName, Func<Game, T> func)
        {
            return Task.Run(() =>
            {
                if (games.Contains(gameName))
                {
                    try 
                    {
                        return func(games[gameName]);
                    }
                    catch (GameException e)
                    {
                        throw new HubException($"Action on game with name {gameName} failed with message {e.Message}");
                    }
                }
                else
                {
                    throw new HubException("Game with that name doesnt exist.");
                }
            });
        }

        private Player GetPlayer()
        {
            return players[Context.ConnectionId];
        }
#endregion
        
#region IGameObserver implementation
        public void OnGameStateChange(string gameName, Game.State gameState)
        {
            Clients.Group(gameName).SendGameStateChange(gameState);
        }

        public void OnCellsRevealed(string gameName, IEnumerable<Cell> cells)
        {
            Clients.Group(gameName).SendRevealedCells(cells);
        }

        public void OnPlayerJoined(string gameName, Player player)
        {
            Clients.Group(gameName).SendPlayerJoined(player);
        }

        public void OnLeaderboardUpdate(string gameName, Player player, int score)
        {
            Clients.Group(gameName).SendLeaderboardUpdate(player, score);
        }
        #endregion
    }
}
