﻿#nullable enable

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Threading.Tasks;

namespace MinesweepeR
{
    public class Board
    {
        public int BoardSize { get; private set;  }
        private readonly Cell[,] grid;

        public Board(int entropy, int boardSize = 20)
        {
            this.BoardSize = boardSize;
            grid = new Cell[boardSize, boardSize];

            for (int x = 0; x < grid.GetLength(0); ++x)
            for (int y = 0; y < grid.GetLength(1); ++y)
            {
                grid[x, y] = new Cell(x, y, chanceOfBeingBomb: entropy > 0 ? entropy / 100d : Double.Epsilon);
            }
        }

        private bool WithinBounds(int x, int y) => 
            (x >= 0 && x < grid.GetLength(0) && y >= 0  && y < grid.GetLength(1));

        private void HandleNeighbours(Cell cell, Action<Cell> onEachNeighbour)
        {
            for(int xOffset = -1; xOffset <= 1; xOffset++)
            for(int yOffset = -1; yOffset <= 1; yOffset++)
            {
                int xNeighbour = cell.X - xOffset;
                int yNeighbour = cell.Y - yOffset;
                bool isSelf = (xOffset == 0 && yOffset == 0);

                if (WithinBounds(xNeighbour, yNeighbour) && !isSelf)
                    onEachNeighbour(grid[xNeighbour, yNeighbour]);
            }
        }

        private int CountNeighbouringBombs(Cell cell)
        {
            if (cell.IsBomb)
                return -1;

            int total = 0;

            HandleNeighbours(cell, (neighbour) =>
            {
                if (neighbour.IsBomb)
                    ++total;
            });

            return total;
        }

        public IEnumerable<Cell> RevealCell(int x, int y, Action? onBomb = null)
        {
            IEnumerable<Cell> revealedCells = new List<Cell>();
            Cell cell = grid[x, y];

            if (onBomb != null && cell.IsBomb) onBomb();

            var neighbouringBombs = CountNeighbouringBombs(cell);
            // Flood fill if no neighbouring bombs
            cell.Reveal(neighbouringBombs);
            revealedCells = revealedCells.Append(cell);
            if (neighbouringBombs == 0)
            {
                HandleNeighbours(cell, (neighbour) =>
                {
                    if (!neighbour.IsBomb && !neighbour.Revealed)
                    {
                        // TODO: We might want to do something different here.
                        // Suspected to be quite memory intensive.
                        // Instead we should do something more graceful and only use one array of some sort
                        revealedCells = revealedCells.Concat(RevealCell(neighbour.X, neighbour.Y));
                    }
                });
            }

            return revealedCells;
        }

        public bool AreAllCellsRevealed()
        {
            for (int x = 0; x < grid.GetLength(0); ++x)
            for (int y = 0; y < grid.GetLength(1); ++y)
                if (!grid[x, y].Revealed)
                    return false;

            return true;
        }
    }
}
