﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace MinesweepeR
{
    public class Leaderboard
    {
        public IDictionary<string, int> Entries { get; }

        public Leaderboard(params Player[] players) 
            => Entries = new Dictionary<string, int>(players.Select(p => new KeyValuePair<string, int>(p.Id, 0)));

        public void AddPoints(Player player, int points)
        {
            if (Entries.ContainsKey(player.Id))
            {
                Entries[player.Id] += points;
            }
            else
            {
                Entries.Add(player.Id, points);
            }
        }

        public void AddPlayer(Player player)
        {
            Entries.TryAdd(player.Id, 0);

        }

        public void RemovePoints(Player player, int points)
        {
            Entries[player.Id] -= points;
        }
    }
}