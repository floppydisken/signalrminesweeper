﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MinesweepeR
{
    public class Game
    {
        public enum State
        {
            NotStarted,
            Running,
            GameOver
        }


        private State gameState;
        public State GameState
        {
            get => gameState;
            private set
            {
                gameState = value;
                gameObserver.OnGameStateChange(name, gameState);
            }
        }
        private readonly ISet<Player> players;
        public IEnumerable<Player> Players => players;

        private readonly IGameObserver gameObserver;

        private readonly Player owner;
        private readonly Board board;
        private readonly string name;
        private readonly int entropy;
        public Leaderboard Leaderboard { get; private set; }

        public int BoardSize
        {
            get
            {
                lock(boardLock) 
                    return board.BoardSize;
            }
        }

        private readonly object boardLock = new object();

        public Game(string name, int entropy, int gridSize, Player owner, IGameObserver gameObserver)
        {
            this.name = name;
            this.entropy = entropy;
            this.owner = owner;
            this.gameObserver = gameObserver;
            
            GameState = State.NotStarted;
            
            gameObserver.OnGameStateChange(this.name, GameState);
            
            board = new Board(this.entropy, gridSize);
            players = new HashSet<Player>();
            Leaderboard = new Leaderboard(players.ToArray());
        }

        /// <summary>
        /// Reveal a cell and return revealed cells
        /// </summary>
        /// <param name="player"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns>Revealed cells</returns>
        public void RevealCell(Player player /*No current use, potential for scoreboard*/, int x, int y)
        {
            lock (boardLock)
            {
                switch (GameState)
                {
                    case State.Running:
                        try
                        {
                            if (!player.IsDead)
                            {
                                IEnumerable<Cell> revealedCells = board.RevealCell(x, y, () => player.IsDead = true);
                                Leaderboard.AddPoints(player, revealedCells.Count());
                                gameObserver.OnLeaderboardUpdate(name, player, revealedCells.Count());
                                gameObserver.OnCellsRevealed(name, revealedCells);
                            }

                            if (IsDone())
                                GameState = State.GameOver;
                        }
                        catch (Exception e)
                        {
                            throw new GameException(GameState, $"Failed with message: {e.Message}");
                        }
                        break;
                    default:
                        throw new GameException(GameState);
                }
            }
        }

        public void JoinGame(Player player)
        {
            if (GameState == State.NotStarted)
            {
                players.Add(player);
                gameObserver.OnPlayerJoined(name, player);
                Leaderboard.AddPlayer(player);
            }
            else
                throw new GameException(GameState);
        }

        public void LeaveGame(Player player)
        {
            players.Remove(player);
        }

        private bool AreAllPlayersDead() => Players.All(player => player.IsDead);

        public bool IsDone()
        {
            lock (boardLock)
            {
                return board.AreAllCellsRevealed() || AreAllPlayersDead();
            }
        }
        
        public void StartGame(Player player)
        {
            if (IsOwner(player) && GameState == State.NotStarted)
                GameState = State.Running;
            else
                throw new GameException(GameState, "Player is not the owner or wrong game state.");
        }

        public bool IsOwner(Player player)
        {
            return owner.Equals(player);
        }
    }
}
