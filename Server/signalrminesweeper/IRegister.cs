﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MinesweepeR
{
    public interface IRegister<IdType, Type>
    {
        Type this[IdType uid] { get; set; }
        bool Contains(IdType uid);
        void Remove(IdType uid);
    }
}
