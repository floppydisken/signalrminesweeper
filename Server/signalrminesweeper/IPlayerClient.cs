﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MinesweepeR
{
    public interface IPlayerClient
    {
        Task SendRevealedCells(IEnumerable<Cell> cells);
        Task SendGameStateChange(Game.State gameState);
        Task SendPlayerJoined(Player player);
        Task SendLeaderboardUpdate(Player player, int newScore);
    }
}
