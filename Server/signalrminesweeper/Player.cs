﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MinesweepeR
{
    public class Player
    {
        public string Id { get; set; }
        public string Alias { get; set; } = "Anon";
        public bool IsDead { get; set; }
    }
}
